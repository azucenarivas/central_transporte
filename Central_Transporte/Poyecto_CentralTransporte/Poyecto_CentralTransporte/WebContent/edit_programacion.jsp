<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />

	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>
				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Actualizar
						Programacion de Salida</FONT></u>
			</h3>

		</div>
	</div>
	<br>

	<form action="actualizarProgramacion" method="post">
		<br>
			<div class="row center">
				<div class="col m2"></div>
				<input type="hidden" name="id" value="${listaID.idProgramaciones }">
				<div class="col m4">
					<h5>Fecha de Programacion:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="fecha" type="date" pattern="yyyy-MM-dd" size="60"
						value="${listaID.fecha }" />
				</div>
				<div class="col m3"></div>
			</div>
			<br>
			 <div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Selecione la unidad:</h5>
				</div>
				<br>
				<div class="col m3">
					<select name="idunidades">
						<option label="Seleccione una Unidad" value="">Seleccione
							un..></option>
						<c:forEach items="${listaU}" var="u">
						<c:choose>
						<c:when test="${u.idUnidades == listaID.unidades.idUnidades}">
						<option value="${u.idUnidades}" selected="">${u.ruta}</option>
						</c:when>
						<c:otherwise>
						<option value="${u.idUnidades}">${u.ruta}</option>
						</c:otherwise>
						</c:choose>
							
						</c:forEach>
					</select>
				</div>
				<div class="col m3"></div>
			</div> 
			<br>


			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Selecione la Persona a Conducir la Unidad:</h5>
				</div>
				<br>
				<div class="col m3">
					<select name="idmotoristas">
						<option label="Seleccione una Persona" value="">Seleccione
							un..></option>
						<c:forEach items="${listaM}" var="p">
						<c:choose>
						<c:when test="${p.idMotoristas == listaID.motoristas.idMotoristas}">
						<option value="${p.idMotoristas}" selected="">${p.nombre}
								${p.apellido}</option>
						</c:when>
						<c:otherwise>
						<option value="${p.idMotoristas}">${p.nombre}
								${p.apellido}</option>
						</c:otherwise>
						</c:choose>
						</c:forEach>
					</select>
				</div>

				<div class="col m3"></div>
			</div>
			<br>


			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese el Punto de Salida:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="puntoSalida" type="text" size="30"
						value="${listaID.puntoSalida }" />
				</div>
				<div class="col m3"></div>
			</div>
			<br>


			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese la Hora de Salida:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="horaSalida" type="time" size="90" value="${listaID.horaSalida }"/> 
				</div>
				<div class="col m3"></div>
			</div>
			<br>

			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese el Punto de LLegada:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="puntoLlegada" type="text" size="30"
						value="${listaID.puntoLlegada }" />
				</div>
				<div class="col m3"></div>
			</div>
			<br>


			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese la hora de LLegada:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="horaLlegada" type="time" size="60"  value="${listaID.horaLlegada }" /> 
				</div>
				<div class="col m3"></div>
			</div>
			<br>

			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Selecione el due�o del Bus:</h5>
				</div>
				<br>
				<div class="col m3">
					<select name="idpersonas">
						<option label="Seleccione una Persona" value="">Seleccione
							un..></option>
						<c:forEach items="${listaP}" var="d">
						<c:choose>
						<c:when test="${d.idPersonas == listaID.personas.idPersonas}">
						<option value="${d.idPersonas}" selected=" ">${d.nombre} ${d.apellido}</option>
						</c:when>
						<c:otherwise>
						<option value="${d.idPersonas}">${d.nombre} ${d.apellido}</option>
						</c:otherwise>
						</c:choose>
							
						</c:forEach>
					</select>
				</div>
				<div class="col m3"></div>
			</div>
			<br>



			<div class="row">
				<div class="col m3"></div>

				<div class="col m3"></div>
				<div class="col m3">
					<button type="submit" class="btn btn-info btn-lg">Guardar
						Datos</button>
				</div>
				<div class="col m3"></div>
			</div>
	</form>


</body>
</html>