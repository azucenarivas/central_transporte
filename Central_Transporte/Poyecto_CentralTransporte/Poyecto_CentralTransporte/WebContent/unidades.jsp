<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />
	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>
				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Listado de
						Unidades Registrada</FONT></u>
				<!--  <h1>Listado de Personas Registradas</h1>-->


			</h3>

		</div>

	</div>
	<br>



	<form action="insUnidades" method="get">
		<button class="btn btn-info">Agregar un Nuevo Registro</button>
	</form>



	<table class="table  table-striped">
		<thead class="thead-dark">
			<tr>
				<th style="text-align: center">Correlativo</th>
				<th style="text-align: center">Placa</th>
				<th style="text-align: center">Capacidad</th>
				<th style="text-align: center">Ruta</th>
				<th style="text-align: center">Encargado de la Unidad</th>
				<th style="text-align: center">Estado</th>
				<th style="text-align: center">Opciones</th>

			</tr>
		</thead>
		<%
			int x = 1;
		%>

		<c:forEach items="${listaU }" var="uni">

			<tr>
				<td style="text-align: center"><%=x%></td>
				<td style="text-align: center">${uni.placa }</td>
				<td style="text-align: center">${uni.capacidad }</td>
				<td style="text-align: center">${uni.ruta }</td>
				<td style="text-align: center">${uni.personas.nombre } ${uni.personas.apellido }</td>
				<td style="text-align: center">${uni.estado }</td>
				<td style="text-align: center">

					<form action="cargarporIDUnidades" method="get">
						<input name="id" value="${uni.idUnidades}" hidden>
						<Button class="btn btn-outline-success">
							<i class="fa fa-remove" style="color: #757575; font-size: 10px"
								style="color:red"> </i> Actualizar
						</Button>
					</form>
				</td>

			</tr>
			<%
				x++;
			%>

		</c:forEach>
	</table>

</body>
</html>