<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 <jsp:include page="menu_encabezado.jsp"/>


        <div class="row">
            <div class="col m12">
                <div class="card black white-text center-align z-depth-5">
                    
                    <FONT FACE="Impact" >
                    <!--<marquee BGCOLOR="#eceff1 "><h2>BIENVENIDOS/AS A TU TERMINAL NUEVOS HORIZONTES </h2></marquee>
                   -->
                    </FONT>


                </div>
            </div>

        </div>


        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://lh3.googleusercontent.com/kCU_Mju93gskwi6shtewXgBDhLIud7LspmiDeftgQgHzFbivI4G5_KNMiYZ2_pbJqd50rQO5=w1080-h608-p-no-v0" width="300" height="700" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://www.elmetropolitanodigital.com/wp-content/uploads/2018/08/cd2fac46-a549-494b-9f60-8cd9e07b2b47.jpg" width="300" height="700" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="https://static.elmundo.sv/wp-content/uploads/2019/03/Terminal-de-buses-2.jpg" width="300" height="750" class="d-block w-100" alt="...">
                </div>

                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
</body>
</html>