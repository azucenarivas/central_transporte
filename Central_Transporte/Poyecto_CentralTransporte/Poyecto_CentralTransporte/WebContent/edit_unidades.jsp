<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />

	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>
				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Actualizar
						Unidades</FONT></u>
			</h3>

		</div>
	</div>
	<br>

	<form action="actualizarUnidades" method="post">
		<br>

		<div class="row center">
			<div class="col m2"></div>
			<input type="hidden" name="id" value="${listaID.idUnidades }">
			<div class="col m4">
				<h5>Ingrese la PLaca de la Unidad:</h5>
			</div>
			<br>
			<div class="col m3">
				<input name="placa" type="text" size="20" maxlength="8" required value="${listaID.placa }" />
			</div>
			<div class="col m3"></div>
		</div>
		<br>


		<div class="row center">
			<div class="col m2"></div>
			<div class="col m4">
				<h5>Capacidad de Pasajeros:</h5>
			</div>
			<br>
			<div class="col m3">
				<input name="capacidad" type="number" max="90" required
					value="${listaID.capacidad }" />
			</div>
			<div class="col m3"></div>
		</div>
		<br>


		<div class="row center">
			<div class="col m2"></div>
			<div class="col m4">
				<h5>Ingrese la ruta:</h5>
			</div>
			<br>
			<div class="col m3">
				<input name="ruta" size="30" type="number" maxlength="999" required value="${listaID.ruta }" />
			</div>
			<div class="col m3"></div>
		</div>
		<br>


		<div class="row center">
			<div class="col m2"></div>
			<div class="col m4">
				<h5>Seleccione el estado de la unidad:</h5>
			</div>
			<div class="col m3">
					<select name="estado" style="width: 210px" required>
					<option>En Recorrido</option>
					<option>En Mantenimiento</option>
				</select>
			</div>
			<div class="col m3"></div>
		</div>
		<br>

		<div class="row center">
			<div class="col m2"></div>
			<div class="col m4">
				<h5>SEleccione la persona encargada:</h5>
			</div>
			<br>
			<div class="col m3">
					<select name="idpersonas" style="width: 210px" required>
					<option label="Seleccione una opci�n" value="">Seleccione
						una opci�n></option>

					<c:forEach items="${listaP}" var="p">
						<c:choose>
							<c:when test="${p.idPersonas == listaID.personas.idPersonas}">
								<option value="${p.idPersonas}" selected="">${p.nombre}
									${p.apellido}</option>
							</c:when>
							<c:otherwise>
								<option value="${p.idPersonas}">${p.nombre}
									${p.apellido}</option>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</select>
			</div>
			<div class="col m3"></div>
		</div>
		<br>



		<div class="row">
			<div class="col m3"></div>

			<div class="col m3"></div>
			<div class="col m3">
				<button type="submit" class="btn btn-info btn-lg">Guardar
					Datos</button>
			</div>
			<div class="col m3"></div>
		</div>
</body>
</html>