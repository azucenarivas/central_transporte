<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:include page="menu_encabezado.jsp"/>

<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>

				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Listado de
						Motoristas Registrados</FONT></u>
			</h3>

		</div>

	</div>
	<br>
	
	<form action="insertarMotorista" method="post">
	<button class="btn btn-info">Agregar un Nuevo Registro</button>
	</form>
	

	<table class="table table-hover" style="width: 100%">
		<thead class="thead-dark">
		<tr>
			<th style="text-align: center">Correlativo</th>
			<th style="text-align: center">Nombre Completo</th>
			<th style="text-align: center">Telefono</th>
			<th style="text-align: center">Dui</th>
			<th style="text-align: center">Licencia</th>
			<th style="text-align: center">Direcci�n</th>
			<th style="text-align: center">Opciones</th>
		</tr>
		</thead>
		<%
			int x = 1;
		%>
		<c:forEach items="${listaM }" var="moto">

			<tr>
				<td style="text-align: center"><%=x%></td>
				<td style="text-align: center">${moto.nombre} ${moto.apellido}</td>
				<td style="text-align: center">${moto.telefono }</td>
				<td style="text-align: center">${moto.dui }</td>
				<td style="text-align: center">${moto.licencia }</td>
				<td style="text-align: center">${moto.direccion }</td>
			
						
                    <td style="text-align: center">
                    
					<form action="caragarporIDMotorista" method="get">
						<input name="id" value="${moto.idMotoristas}" hidden>
						<Button class="btn btn-outline-success">
							<i class="fa fa-remove" style="color: #757575; font-size: 10px"
								style="color:red"> </i> Actualizar
						</Button>
					</form>
					</td>
			</tr>

			<%
				x++;
			%>
		</c:forEach>
	</table>
</body>
</html>