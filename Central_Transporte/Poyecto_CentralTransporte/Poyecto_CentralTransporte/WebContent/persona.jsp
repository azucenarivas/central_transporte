<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />
	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>

				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Listado de
						Personas Registrada</FONT></u>
				<!--  <h1>Listado de Personas Registradas</h1>-->


			</h3>

		</div>

	</div>
	<br>
	<form action="insPersona" method="get">
		<button class="btn btn-outline-info">Agregar un Nuevo Registro</button>
	</form>
	<table class="table table-hover" style="width: 100%">
		<thead class="thead-dark">
			<tr>
				<th style="text-align: center">Correlativo</th>
				<th style="text-align: center">Nombre Completo</th>
				<th style="text-align: center">Telefono</th>
				<th style="text-align: center">Dui</th>
				<th style="text-align: center">Direccion</th>
				<th style="text-align: center"></th>
				<th style="text-align: center"></th>
			</tr>
		</thead>
		<%
			int x = 1;
		%>
		<c:forEach items="${listaP}" var="per">

			<tr class="table-light">
				<td style="text-align: center"><%=x%></td>
				<td style="text-align: center">${per.nombre}   ${per.apellido }</td>
				<td style="text-align: center">${per.telefono }</td>
				<td style="text-align: center">${per.dui }</td>
				<td style="text-align: center">${per.direccion }</td>
				<td style="text-align: center">
				<a
					href="eliminarDatosPersona/${per.idPersonas}"
					class="btn btn-outline-danger"><i class="fa fa-remove"
						style="color: #757575; font-size: 10px" style="color:red"></i>
						Eliminar</a>
						</td>
						
						
						
                    <td style="text-align: center">
                    
					<form action="caragarporIDPersona" method="get">
						<input name="id" value="${per.idPersonas}" hidden>
						<Button class="btn btn-outline-success">
							<i class="fa fa-remove" style="color: #757575; font-size: 10px"
								style="color:red"> </i> Actualizar
						</Button>
					</form>
					</td>
			</tr>
			<%
				x++;
			%>

		</c:forEach>
	</table>




</body>

</html>