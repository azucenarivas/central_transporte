<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />
	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>

				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Programación de Salidas por Fechas</FONT></u>
			</h3>

		</div>
	</div>
	<br>
		
		<div class="row center">
			<form action="verfechasProgramacion" method="get">
				<div class="col m6">
					<h4>Selecione una fecha:</h4>
				</div>
				<div class="col m3">
					<select name="fechas" id="opciones" required>
						<option value=""></option>
						<c:forEach items="${listaPro}" var="f">
							<option  data-pro="${f.fecha}" value="${f.idProgramaciones}">${f.fecha}</option>
						</c:forEach>

					</select>
				</div>
				<br><br>
				<!--  <div class="col m3">
					 <input class="btn btn-info" type="submit" value="Cargar Datos" />

				</div>
				
				
	<table class="table  table-striped">
		<thead class="thead-dark">
			<tr>
				<th style="text-align: center">Correlativo</th>
				<th style="text-align: center">Fecha</th>
				<th style="text-align: center">Ruta</th>
				<th style="text-align: center">Punto de Salida</th>
				<th style="text-align: center">Hora de Salida</th>
				<th style="text-align: center">Punto de LLegada</th>
				<th style="text-align: center">Hora de Llegada</th>

			</tr>
		</thead>
		</table>-->
			</form>
		</div>
</body>
</html>