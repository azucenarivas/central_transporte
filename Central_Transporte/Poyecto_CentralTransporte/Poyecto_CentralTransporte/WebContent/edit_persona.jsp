<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />

	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>

				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31"> Actualizar
						Registro de Encargado de Las Unidades de Transporte</FONT></u>
			</h3>

		</div>
	</div>
	<br>
	<form action="actualizarPersona" method="post">

		<br>
		<c:forEach items="${listaP }" var="p">
			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<!--<input name="id" size="60" value="${p.idPersonas }" /> -->
					<h5>Ingrese Nombre de la persona:</h5>
				</div>
				<br>
				<div class="col m3">
					<input type="hidden" name="id" size="60" value="${p.idPersonas }" />
					<input name="nombre" type="text" size="60" value="${p.nombre }"/>
				</div>
				<div class="col m3"></div>
			</div>
			<br>

			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese Apellido de la persona:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="apellido" type="text" size="60" value="${p.apellido }"/>
				</div>
				<div class="col m3"></div>
			</div>
			<br>


			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese Telefono de la persona:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="telefono" type="text" size="60" value="${p.telefono }" />
				</div>
				<div class="col m3"></div>
			</div>
			<br>

			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese Dui de la persona:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="dui" type="text" size="60" value="${p.dui }"/>
				</div>
				<div class="col m3"></div>
			</div>
			<br>

			<div class="row center">
				<div class="col m2"></div>
				<div class="col m4">
					<h5>Ingrese Direcion de la persona:</h5>
				</div>
				<br>
				<div class="col m3">
					<input name="direccion" type="text" size="60" value="${p.direccion }" />
				</div>
				<div class="col m3"></div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col m3"></div>

				<div class="col m3"></div>
				<div class="col m3">
					<button type="submit" class="btn btn-info btn-lg">Guardar
						Datos</button>
				</div>
				<div class="col m3"></div>
			</div>
		</c:forEach>
	</form>

</body>
</html>