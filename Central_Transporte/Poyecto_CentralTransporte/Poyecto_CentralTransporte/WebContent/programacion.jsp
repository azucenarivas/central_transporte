<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="menu_encabezado.jsp" />

	<br>

	<div class="col-md-12">
		<div class="col m12" align="center">
			<h3>

				<u><FONT FACE="impact" SIZE=6 COLOR="#363b31">Lista de
						programacion de Salidas de Unidades de Transporte Colectivo</FONT></u>
			</h3>

		</div>

	</div>
	<br>


	<br>
	<form action="insProgramacion" method="get">
		<button class="btn btn-info">Agregar un Nuevo Registro</button>
	</form>



	<table class="table  table-striped">
		<thead class="thead-dark">
			<tr>
				<th style="text-align: center">#</th>
				<th>Fecha</th>
				<th style="text-align: center">Unidades</th>
				<th style="text-align: center">Motorista</th>
				<th style="text-align: center">Punto de Salida</th>
				<th style="text-align: center">Hora de Salida</th>
				<th style="text-align: center">Punto de LLegada</th>
				<th style="text-align: center">Hora de LLegada</th>
				<th style="text-align: center">Due�o del bus</th>
				<th style="text-align: center">Acci�n</th>
			</tr>
		</thead>
		<%
			int x = 1;
		%>

		<c:forEach items="${listaPro }" var="pro">

			<tr>
				<td style="text-align: center"><%=x%></td>
				<td>${pro.fecha}</td>
				<td style="text-align: center">${pro.unidades.ruta }</td>
				<td>${pro.motoristas.nombre} ${pro.motoristas.apellido}</td>
				<td style="text-align: center">${pro.puntoSalida }</td>
				<td style="text-align: center">${pro.horaSalida }</td>
				<td style="text-align: center">${pro.puntoLlegada }</td>
				<td style="text-align: center">${pro.horaLlegada }</td>
				<td style="text-align: center">${pro.personas.nombre}
					${pro.personas.apellido}</td>

				<td style="text-align: center">

					<form action="cargarporIDProgramacion" method="get">
						<input name="idd" value="${pro.idProgramaciones}" hidden>
						<Button class="btn btn-outline-success">
							<i class="fa fa-remove" style="color: #757575; font-size: 10px"
								style="color:red"> </i> Actualizar
						</Button>
					</form>
				</td>
			</tr>

			<%
				x++;
			%>
		</c:forEach>
	</table>
</body>
</html>