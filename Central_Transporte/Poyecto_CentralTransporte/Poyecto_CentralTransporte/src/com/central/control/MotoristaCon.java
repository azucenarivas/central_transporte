package com.central.control;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.central.imp.MotoristaRep;
import com.central.models.Motoristas;
import com.central.models.Personas;
@Controller
public class MotoristaCon {
	@Autowired
	@Qualifier("motoristaR")
	private MotoristaRep motorista;
	
	@RequestMapping(value = "/verMotorista", method = RequestMethod.GET)
	public ModelAndView vistaMotorista() {
		//String msj="Lista de Motorista";
		ModelAndView mv=new ModelAndView();
		mv.addObject("listaM", motorista.read());
		mv.setViewName("motorista");
		return mv;
	
		
	}
	
	
	@RequestMapping(value = "/insertarMotorista", method = RequestMethod.POST)
	public ModelAndView insertarMotorista(@RequestParam("nombre") String nombre,
			@RequestParam("apellido") String apellido, @RequestParam("telefono") String telefono,
			@RequestParam("dui") String dui, @RequestParam("direccion") String direccion, @RequestParam("licencia") String licencia) {
		ModelAndView mv = new ModelAndView();


		Motoristas m = new Motoristas();
		m.setIdMotoristas(0);
		m.setNombre(nombre);
		m.setApellido(apellido);
		m.setTelefono(telefono);
		m.setDui(dui);
		m.setDireccion(direccion);
		m.setLicencia(licencia);
		motorista.create(m);
		mv.addObject("listaM", motorista.read());
		mv.setViewName("motorista");
		return mv;

	}
	
	///metodo para cargar datos de personas por id

		@RequestMapping(value = "/caragarporIDMotorista", method = RequestMethod.GET)
		public ModelAndView cargarDatosporID(@RequestParam("id") int id) {
			ModelAndView mv = new ModelAndView();
			List<Motoristas> lista = new LinkedList<>();
			try {
				Motoristas per = new Motoristas();
				per.setIdMotoristas(id);
				per = motorista.readById(per.getIdMotoristas());
				lista.add(per);
				mv.addObject("listaID", motorista.readById(id));
				mv.addObject("listaM", motorista.read());
				mv.setViewName("edit_motoristas");
			} catch (Exception e) {
				lista = motorista.read();
				mv.addObject("listaM", lista);
				mv.setViewName("motorista");
			}
			return mv;

		}
		
		
		@RequestMapping(value = "/actualizarMotorista", method = RequestMethod.POST)
		public ModelAndView actualizarMotorista(@RequestParam("id") int id, @RequestParam("nombre") String nombre,
				@RequestParam("apellido") String apellido, @RequestParam("telefono") String telefono,
				@RequestParam("dui") String dui, @RequestParam("direccion") String direccion, @RequestParam("licencia") String licencia) {
			ModelAndView mv = new ModelAndView();

			Motoristas m = new Motoristas();
			m.setIdMotoristas(id);
			m.setNombre(nombre);
			m.setApellido(apellido);
			m.setTelefono(telefono);
			m.setDui(dui);
			m.setDireccion(direccion);
			m.setLicencia(licencia);
			motorista.update(m);
			mv.addObject("listaM", motorista.read());
			mv.setViewName("motorista");
			return mv;

		}
		
		
		
	

}
