package com.central.control;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.central.imp.PersonaRep;
import com.central.models.Personas;

@Controller
public class PersonasCon {
	@Autowired
	@Qualifier("personaR")
	private PersonaRep perso;

	@RequestMapping(value = "/verPersona", method = RequestMethod.GET)
	public ModelAndView vistaPersona() {
		String msj = "Lista de Personas";
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return mv;

	}

	//// metodo para regresar a la vista de mostrar
	@RequestMapping(value = "/insPersona", method = RequestMethod.GET)
	public ModelAndView regPersona() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("insert_personas");
		return mv;
	}

	@RequestMapping(value = "/insertarPersona", method = RequestMethod.POST)
	public ModelAndView registrarPersona(@RequestParam("nombre") String nombre,
			@RequestParam("apellido") String apellido, @RequestParam("telefono") String telefono,
			@RequestParam("dui") String dui, @RequestParam("direccion") String direccion) {
		ModelAndView mv = new ModelAndView();

		Personas p = new Personas();
		p.setIdPersonas(0);
		p.setNombre(nombre);
		p.setApellido(apellido);
		p.setTelefono(telefono);
		p.setDui(dui);
		p.setDireccion(direccion);
		perso.create(p);
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return mv;

	}
	
///metodo para cargar datos de personas por id

	@RequestMapping(value = "/caragarporIDPersona", method = RequestMethod.GET)
	public ModelAndView cargarDatosporID(@RequestParam("id") int id) {
		ModelAndView mv = new ModelAndView();
		List<Personas> lista = new LinkedList<>();
		try {
			Personas per = new Personas();
			per.setIdPersonas(id);
			per = perso.readById(per.getIdPersonas());
			lista.add(per);
			mv.addObject("listaP", lista);
			mv.setViewName("edit_persona");
		} catch (Exception e) {
			lista = perso.read();
			mv.addObject("listaP", lista);
			mv.setViewName("persona");
		}
		return mv;

	}

	/////////// metodo para actualizar

	@RequestMapping(value = "/actualizarPersona", method = RequestMethod.POST)
	public ModelAndView actualizaPersona(@RequestParam("id") int id, @RequestParam("nombre") String nombre,
			@RequestParam("apellido") String apellido, @RequestParam("telefono") String telefono,
			@RequestParam("dui") String dui, @RequestParam("direccion") String direccion) {
		ModelAndView mv = new ModelAndView();

		Personas p = new Personas();
		p.setIdPersonas(id);
		p.setNombre(nombre);
		p.setApellido(apellido);
		p.setTelefono(telefono);
		p.setDui(dui);
		p.setDireccion(direccion);
		perso.update(p);
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return mv;

	}

	/// metodo para eliminar


	/*@RequestMapping(value = "/eliminarDatosPersona/{id}", method = RequestMethod.GET)
	public ModelAndView eliminarDatospersona(@PathVariable("id") int id) {
		ModelAndView mv = new ModelAndView();
		Personas per = new Personas();
		per.setIdPersonas(id);
		Personas sql = this.perso.readById(id);
		this.perso.delete(sql);
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return mv;
	}*/
	
	
	@RequestMapping(value = "/eliminarDatosPersona/{id}", method = RequestMethod.GET)
	public String eliminarDatospersona(@PathVariable("id") int id) {
		ModelAndView mv = new ModelAndView();
		Personas per = new Personas();
		per.setIdPersonas(id);
		Personas sql = this.perso.readById(id);
		this.perso.delete(sql);
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return "redirect:/regresar";
	}
	
////metodo para regresar a la vista de mostrar
	@RequestMapping(value = "/regresar", method = RequestMethod.GET)
	public ModelAndView regRegresar() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaP", perso.read());
		mv.setViewName("persona");
		return mv;
	}
}
