package com.central.control;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.central.imp.MotoristaRep;
import com.central.imp.PersonaRep;
import com.central.imp.ProgramacionSalidasRep;
import com.central.imp.UnidadesRep;
import com.central.models.Motoristas;
import com.central.models.Personas;
import com.central.models.ProgramacionSalidas;
import com.central.models.Unidades;

@Controller
public class ProgramacionCon {

	@Autowired
	@Qualifier("programaR")
	private ProgramacionSalidasRep pro;

	@Autowired
	@Qualifier("motoristaR")
	private MotoristaRep moto;

	@Autowired
	@Qualifier("unidadesR")
	private UnidadesRep uni;

	@Autowired
	@Qualifier("personaR")
	private PersonaRep perso;

	@RequestMapping(value = "/tablaPro", method = RequestMethod.GET)
	public ModelAndView vistaProgramacion() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaPro", pro.read());
		mv.setViewName("programacion");
		return mv;

	}

	//visualizo los datos(cargar en tabla)
	@RequestMapping(value = "/insProgramacion", method = RequestMethod.GET)
	public ModelAndView regProgramacion() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaPro", pro.read());
		mv.addObject("listaU", uni.read());
		mv.addObject("listaM", moto.read());
		mv.addObject("listaP", perso.read());
		mv.setViewName("insert_programacion_salida");
		return mv;

	}
	
	///inserto datos

	@RequestMapping(value = "/insertarProgramacion", method = RequestMethod.GET)
	public ModelAndView registrarProgramacion(@RequestParam("fecha") String fecha,
			@RequestParam("idunidades") int idunidades, @RequestParam("idmotoristas") int idmotoristas,
			@RequestParam("puntoSalida") String puntoSalida, @RequestParam("horaSalida") String horaSalida,
			@RequestParam("puntoLlegada") String puntoLlegada, @RequestParam("horaLlegada") String horaLlegada,
			@RequestParam("idpersonas") int idpersonas) {
		ModelAndView mv = new ModelAndView();
		try {
			ProgramacionSalidas p = new ProgramacionSalidas();
			Motoristas m = new Motoristas();
			Personas pe = new Personas();
			Unidades u = new Unidades();

			p.setIdProgramaciones(0);
			Date fe = java.sql.Date.valueOf(fecha);
			p.setFecha(fe);
			u.setIdUnidades(idunidades);
			p.setUnidades(u);
			m.setIdMotoristas(idmotoristas);
			p.setMotoristas(m);
			p.setPuntoSalida(puntoSalida);
			p.setHoraSalida(horaSalida);
			p.setPuntoLlegada(puntoLlegada);
			p.setHoraLlegada(horaLlegada);
			pe.setIdPersonas(idpersonas);
			p.setPersonas(pe);
			pro.create(p);

		} catch (Exception e) {
			// TODO: handle exception
		}
		mv.addObject("listaPro", pro.read());
		mv.setViewName("programacion");
		return mv;

	}
	
	///metodo para cargar datos por ID
	
	@RequestMapping(value = "/cargarporIDProgramacion", method = RequestMethod.GET)
	public ModelAndView cargarDatosporID(@RequestParam("idd") int id) {
		ModelAndView mv=new ModelAndView();
		List<ProgramacionSalidas> lista=new LinkedList<>();
		try {
			ProgramacionSalidas po=new ProgramacionSalidas();
			po.setIdProgramaciones(id);
			lista.add(po);
			mv.addObject("listaID", pro.readById(id));
			mv.addObject("listaPro",pro.read());
			mv.addObject("listaU", uni.read());
			mv.addObject("listaM", moto.read());
			mv.addObject("listaP", perso.read());
			mv.setViewName("edit_programacion");
		} catch (Exception e) {
			lista=pro.read();
			mv.addObject("listaPro", lista);
			mv.setViewName("programacion");
			
		}
		return mv;
		
	}
	
	
	///actualizar datos

		@RequestMapping(value = "/actualizarProgramacion", method = RequestMethod.POST)
		public ModelAndView actualizarProgramacion(@RequestParam("id") int id, @RequestParam("fecha") String fecha,
				@RequestParam("idunidades") int idunidades, @RequestParam("idmotoristas") int idmotoristas,
				@RequestParam("puntoSalida") String puntoSalida, @RequestParam("horaSalida") String horaSalida,
				@RequestParam("puntoLlegada") String puntoLlegada, @RequestParam("horaLlegada") String horaLlegada,
				@RequestParam("idpersonas") int idpersonas) {
			ModelAndView mv = new ModelAndView();
			try {
				ProgramacionSalidas p = new ProgramacionSalidas();
				Motoristas m = new Motoristas();
				Personas pe = new Personas();
				Unidades u = new Unidades();

				p.setIdProgramaciones(id);
				Date fe = java.sql.Date.valueOf(fecha);
				p.setFecha(fe);
				u.setIdUnidades(idunidades);
				p.setUnidades(u);
				m.setIdMotoristas(idmotoristas);
				p.setMotoristas(m);
				p.setPuntoSalida(puntoSalida);
				p.setHoraSalida(horaSalida);
				p.setPuntoLlegada(puntoLlegada);
				p.setHoraLlegada(horaLlegada);
				pe.setIdPersonas(idpersonas);
				p.setPersonas(pe);
				pro.update(p);

			} catch (Exception e) {
				// TODO: handle exception
			}
			mv.addObject("listaPro", pro.read());
			mv.setViewName("programacion");
			return mv;

		}
		
		///metodo para ver las asignaciones por fecha
		@RequestMapping(value = "/verfechasProgramacion", method = RequestMethod.GET)
		public ModelAndView fechaseleccionada(@RequestParam("fecha")String fechas) {
			ModelAndView mv=new ModelAndView();
			try {
				List<ProgramacionSalidas>lista=new LinkedList<>();
				lista=pro.consultarporFechasProgramacionSelecionada(fechas);
				mv.addObject("listaPro", lista);
				mv.setViewName("busqeda_programacion");
				return mv;
			} catch (Exception e) {
				// TODO: handle exception
			}
			return mv;
		}
			
		
}
