package com.central.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.central.imp.RolesRep;
@Controller
public class RolesCo {
	@Autowired
	@Qualifier("rolesR")
	private RolesRep roles;
	
	@RequestMapping(value = "/tablaR", method = RequestMethod.GET)
	public ModelAndView vistaRoles() {
		String msj="Lista de Roles";
		ModelAndView mv=new ModelAndView();
		mv.addObject("listaR", roles.read());
		mv.setViewName("roles");
		return mv;
	
		
	}
}
