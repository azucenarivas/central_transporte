package com.central.control;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.central.imp.PersonaRep;
import com.central.imp.UnidadesRep;
import com.central.models.Personas;
import com.central.models.ProgramacionSalidas;
import com.central.models.Unidades;

@Controller
public class UnidadesCon {
	@Autowired
	@Qualifier("unidadesR")
	private UnidadesRep uni;

	@Autowired
	@Qualifier("personaR")
	private PersonaRep perso;

	@RequestMapping(value = "/tablaU", method = RequestMethod.GET)
	public ModelAndView vistaUnidades() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaU", uni.read());
		mv.setViewName("unidades");
		return mv;

	}

	@RequestMapping(value = "/insUnidades", method = RequestMethod.GET)
	public ModelAndView regUnidades() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("listaU", uni.read());
		mv.addObject("listaP", perso.read());
		mv.setViewName("insert_unidades");
		return mv;
	}

	
	@RequestMapping(value = "/insertarUnidades", method = RequestMethod.GET)
	public ModelAndView registrarUnidades(@RequestParam("idpersonas") int idpersonas,
			@RequestParam("placa") String placa, @RequestParam("capacidad") int capacidad,
			@RequestParam("ruta") String ruta, @RequestParam("estado") String estado) {
		ModelAndView mv = new ModelAndView();
		try {
			Unidades u = new Unidades();
			Personas p = new Personas();
			u.setIdUnidades(0);
			p.setIdPersonas(idpersonas);
			u.setPersonas(p);
			u.setPlaca(placa);
			u.setCapacidad(capacidad);
			u.setRuta(ruta);
			u.setEstado(estado);
			uni.create(u);

		} catch (Exception e) {
			// TODO: handle exception
		}
		mv.addObject("listaU", uni.read());
		mv.setViewName("unidades");
		return mv;
	}
	
	///metodo para cargar datos de personas por id

	@RequestMapping(value = "/cargarporIDUnidades", method = RequestMethod.GET)
	public ModelAndView cargarDatosporID(@RequestParam("id") int id) {
		ModelAndView mv=new ModelAndView();
		List<Unidades> lista=new LinkedList<>();
		try {
			Unidades unida=new Unidades();
			unida.setIdUnidades(id);
			lista.add(unida);
			mv.addObject("listaID", uni.readById(id));
			mv.addObject("listaU", uni.read());
			mv.addObject("listaP", perso.read());
			mv.setViewName("edit_unidades");
			//System.out.println("haaaaaaaaaaaaaaaaaaaa"+id);
		} catch (Exception e) {
			lista=uni.read();
			mv.addObject("listaU", lista);
			mv.setViewName("unidades");
			
		}
		return mv;
		
	}

		/////////// metodo para actualizar

	@RequestMapping(value = "/actualizarUnidades", method = RequestMethod.POST)
	public ModelAndView actualizarrUnidades(@RequestParam("id") int id,@RequestParam("idpersonas") int idpersonas,
			@RequestParam("placa") String placa, @RequestParam("capacidad") int capacidad,
			@RequestParam("ruta") String ruta, @RequestParam("estado") String estado) {
		ModelAndView mv = new ModelAndView();
		try {
			Unidades u = new Unidades();
			Personas p = new Personas();
			u.setIdUnidades(id);
			p.setIdPersonas(idpersonas);
			u.setPersonas(p);
			u.setPlaca(placa);
			u.setCapacidad(capacidad);
			u.setRuta(ruta);
			u.setEstado(estado);
			uni.update(u);

		} catch (Exception e) {
			// TODO: handle exception
		}
		mv.addObject("listaU", uni.read());
		mv.setViewName("unidades");
		return mv;
	}


}
