package com.central.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.central.imp.UsuarioRep;
@Controller
public class UsuariosCo {
	@Autowired
	@Qualifier("usuarioR")
	private UsuarioRep usua;
	
	@RequestMapping(value = "/tablaUsu", method = RequestMethod.GET)
	public ModelAndView vistaUsuarios() {
		String msj="Lista de Uusarios";
		ModelAndView mv=new ModelAndView();
		mv.addObject("listaUsu", usua.read());
		mv.setViewName("usuario");
		return mv;
	
		
	}

}
