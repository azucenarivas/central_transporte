package com.central.utils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.central.imp.MotoristaRep;
import com.central.imp.PersonaRep;
import com.central.imp.ProgramacionSalidasRep;
import com.central.imp.RolesRep;
import com.central.imp.UnidadesRep;
import com.central.imp.UsuarioRep;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan("com.central")
public class Configuracion {

	@Bean
	InternalResourceViewResolver transporte() {
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;

	}

	@Bean(name = "datasource")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://192.168.101.91:3306/central_transporte_azucena?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;

	}

	@Autowired
	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean startSession() {
		LocalSessionFactoryBean fabrica = new LocalSessionFactoryBean();
		fabrica.setDataSource(dataSource());
		fabrica.setPackagesToScan("com.central.models");
		fabrica.setHibernateProperties(getHibernateProperties());
		return fabrica;
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
		return tx;
	}

	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		// hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}
	
	@Autowired
	@Bean (name = "personaR")
	public PersonaRep persona(SessionFactory sessionFactory) {
		return new PersonaRep(sessionFactory);
	}
	
	@Autowired
	@Bean (name = "motoristaR")
	public MotoristaRep motorista(SessionFactory sessionFactory) {
		return new MotoristaRep(sessionFactory);
	}
	
	@Autowired
	@Bean (name = "programaR")
	public ProgramacionSalidasRep programa(SessionFactory sessionFactory) {
		return new ProgramacionSalidasRep(sessionFactory);
	}
	
	
	@Autowired
	@Bean (name = "rolesR")
	public RolesRep roles(SessionFactory sessionFactory) {
		return new RolesRep(sessionFactory);
	}
	
	
	@Autowired
	@Bean (name = "unidadesR")
	public UnidadesRep uni(SessionFactory sessionFactory) {
		return new UnidadesRep(sessionFactory);
	}
	
	@Autowired
	@Bean (name = "usuarioR")
	public UsuarioRep usua(SessionFactory sessionFactory) {
		return new UsuarioRep(sessionFactory);
	}

}
