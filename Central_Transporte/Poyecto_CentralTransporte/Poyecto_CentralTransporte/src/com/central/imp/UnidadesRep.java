package com.central.imp;


import org.hibernate.SessionFactory;
import com.central.models.Unidades;
import com.central.utils.AbsFacade;
import com.central.utils.DAO;



public class UnidadesRep extends AbsFacade<Unidades> implements DAO<Unidades> {

	SessionFactory starsession;
	public UnidadesRep(SessionFactory sessionFactory) {
		super(Unidades.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}
	

	
	

}
