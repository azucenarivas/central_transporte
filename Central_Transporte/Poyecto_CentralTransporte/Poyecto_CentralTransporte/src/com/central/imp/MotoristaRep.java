package com.central.imp;


import org.hibernate.SessionFactory;

import com.central.models.Motoristas;
import com.central.utils.AbsFacade;
import com.central.utils.DAO;



public class MotoristaRep extends AbsFacade<Motoristas> implements DAO<Motoristas>{

	SessionFactory starsession;
	public MotoristaRep(SessionFactory sessionFactory) {
		super(Motoristas.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}
}
