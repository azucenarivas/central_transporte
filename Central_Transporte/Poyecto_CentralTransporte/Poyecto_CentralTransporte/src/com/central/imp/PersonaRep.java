package com.central.imp;

import org.hibernate.SessionFactory;

import com.central.models.Personas;
import com.central.utils.AbsFacade;
import com.central.utils.DAO;

public class PersonaRep extends AbsFacade<Personas> implements DAO<Personas> {
	SessionFactory starsession;
	public PersonaRep(SessionFactory sessionFactory) {
		super(Personas.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}

}
