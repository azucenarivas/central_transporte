package com.central.imp;


import org.hibernate.SessionFactory;
import com.central.models.Roles;

import com.central.utils.AbsFacade;
import com.central.utils.DAO;



public class RolesRep extends AbsFacade<Roles> implements DAO<Roles> {
	

	SessionFactory starsession;
	public RolesRep(SessionFactory sessionFactory) {
		super(Roles.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}
	

}
