package com.central.imp;


import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.web.bind.annotation.RequestParam;

import com.central.models.ProgramacionSalidas;
import com.central.utils.AbsFacade;
import com.central.utils.DAO;




public class ProgramacionSalidasRep extends AbsFacade<ProgramacionSalidas> implements DAO<ProgramacionSalidas> {

	SessionFactory starsession;
	public ProgramacionSalidasRep(SessionFactory sessionFactory) {
		super(ProgramacionSalidas.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}
	
///METODO ESPECIFICO
	public List<ProgramacionSalidas> consultarProgramacionSalida(String progra) {
		List<ProgramacionSalidas> lista = new LinkedList<>();
		try {
			Query q = getSessionFactory().getCurrentSession().createQuery(
					"select p.id_programaciones,p.fecha,u.ruta,u.placa,m.nombre,m.apellido,p.punto_salida,p.hora_salida,p.punto_llegada,p.hora_llegada\r\n" + 
					"from programacion_salidas p\r\n" + 
					"inner join unidades u on u.id_unidades=p.id_unidades\r\n" + 
					"inner join motoristas m on m.id_motoristas=p.id_motoristas\r\n" + 
					"where  fecha=?1", ProgramacionSalidas.class);
			q.setParameter(1, progra);
			lista = q.getResultList();
		} catch (Exception e) {
		}
		return lista;
	}
	
	
	
	////metod0o para cargar la asignacion de programacion por fechas
	public List<ProgramacionSalidas> consultarporFechasProgramacionSelecionada(String fecha) {
		List<ProgramacionSalidas> lista = new LinkedList<>();
		try {
			String sql = "SELECT * FROM central_transporte_azucena.programacion_salidas p where p.fecha = '"+fecha+"';";
			Query q = getSessionFactory().getCurrentSession().createQuery(
					sql, ProgramacionSalidas.class);
			lista = q.getResultList();
		} catch (Exception e) {
		}
		return lista;
	}
	
	
}
