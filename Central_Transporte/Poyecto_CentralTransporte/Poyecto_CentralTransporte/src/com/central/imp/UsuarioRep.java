package com.central.imp;


import org.hibernate.SessionFactory;
import com.central.models.Usuarios;
import com.central.utils.AbsFacade;
import com.central.utils.DAO;


public class UsuarioRep extends AbsFacade<Usuarios> implements DAO<Usuarios> {

	SessionFactory starsession;
	public UsuarioRep(SessionFactory sessionFactory) {
		super(Usuarios.class);
		this.starsession = sessionFactory;
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return starsession;
	}
	

}
