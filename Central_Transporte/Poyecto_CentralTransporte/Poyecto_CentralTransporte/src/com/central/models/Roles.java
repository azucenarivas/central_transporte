package com.central.models;
// Generated 02-14-2020 08:39:01 AM by Hibernate Tools 5.2.12.Final

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Roles generated by hbm2java
 */
@Entity
@Table(name = "roles", catalog = "central_transporte_azucena")
public class Roles implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idRoles;
	private String cargo;
	private List<Usuarios> usuarioses = new ArrayList<Usuarios>(0);

	public Roles() {
	}

	public Roles(String cargo) {
		this.cargo = cargo;
	}

	public Roles(String cargo, List<Usuarios> usuarioses) {
		this.cargo = cargo;
		this.usuarioses = usuarioses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id_roles", unique = true, nullable = false)
	public Integer getIdRoles() {
		return this.idRoles;
	}

	public void setIdRoles(Integer idRoles) {
		this.idRoles = idRoles;
	}

	@Column(name = "cargo", nullable = false, length = 60)
	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	public List<Usuarios> getUsuarioses() {
		return this.usuarioses;
	}

	public void setUsuarioses(List<Usuarios> usuarioses) {
		this.usuarioses = usuarioses;
	}

}
